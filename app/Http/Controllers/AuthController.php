<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('tugas1.form');
    }
    
    public function kirim(Request $request) {
        $awal = $request['nama_awal'];
        $akhir = $request['nama_akhir'];

        return view('tugas1.welcome', compact('awal','akhir'));
    }

}
