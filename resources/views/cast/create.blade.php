@extends('layout.master')

@section('judul')
Tambah Data
@endsection

@section('konten')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Nama Cast</label>
        <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="title">Email cast</label>
        <input type="text" class="form-control" name="email" id="title" placeholder="Masukkan Email">
        @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        </div>
    <div class="form-group">
        <label for="body">BIO</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10"></textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
</div>
@endsection