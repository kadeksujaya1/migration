@extends('layout.master')

@section('judul')
Edit Data {{$cast->nama}}
@endsection

@section('konten')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan Title">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Email</label>
            <input type="text" class="form-control" name="email" value="{{$cast->email}}" id="title" placeholder="Masukkan Title">
            @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">BIO</label>
            <textarea name="bio" class="form-control" id="" cols="30" rows="10"{{$cast->bio}}></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection